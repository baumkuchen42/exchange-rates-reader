import urllib.request
import urllib.parse
import json


def fetch_current_rates(input_currency: str, result_currency: str):
	url = f'https://api.exchangeratesapi.io/latest?base={input_currency}&symbols={result_currency}'
	with urllib.request.urlopen(url) as response:
		result = response.read().decode('utf-8')
		result_json = json.loads(result)
	return result_json

def get_result_rate(input_currency: str, result_currency: str):
	data = fetch_current_rates(input_currency, result_currency)
	return data['rates'][result_currency]
