# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from .exchange_rates_getter import get_result_rate


@Gtk.Template(resource_path='/de/hszg/exchangerates/window.ui')
class ExchangeratesTask1Window(Gtk.ApplicationWindow):
	__gtype_name__ = 'ExchangeratesTask1Window'

	input_dropdown = Gtk.Template.Child()
	input_currency_label = Gtk.Template.Child()
	result_dropdown = Gtk.Template.Child()
	result_currency_label = Gtk.Template.Child()
	result_amount_label = Gtk.Template.Child()
	conversion_btn = Gtk.Template.Child()

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.connect_actions()
		self.update_result()

	def connect_actions(self):
		self.input_dropdown.connect('changed', self.update_input_currency)
		self.result_dropdown.connect('changed', self.update_result_currency)
		self.conversion_btn.connect('clicked', self.switch_input_result)

	def update_input_currency(self, widget):
		self.input_currency_label.set_text(widget.get_active_text())
		self.update_result()

	def update_result_currency(self, widget):
		self.result_currency_label.set_text(widget.get_active_text())
		self.update_result()

	def switch_input_result(self, widget):
		old_input = self.input_dropdown.get_active()
		self.input_dropdown.set_active(self.result_dropdown.get_active())
		self.result_dropdown.set_active(old_input)

	def update_result(self):
		new_result = get_result_rate(self.input_dropdown.get_active_text(), self.result_dropdown.get_active_text())
		self.result_amount_label.set_text(str(round(new_result, 4)))
