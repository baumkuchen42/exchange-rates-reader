# Exchange Rates Reader

An example application to show how intelligent agents can fetch information from the web.

This agent fetches current exchange rates from https://exchangeratesapi.io/.
